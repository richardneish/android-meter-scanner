# Android meter scanner

Simple app to use the camera and OCR to capture a meter reading.

## Goals
### MVP
  * As a user I can quickly open the app and start scanning for a number
  * When a number is captured I can copy it to the clipboard

### Other Goals
  * Keep a history of readings
  * Export all history
  * Tag readings by type
  * Export all history for a tag
  * Submit a meter reading to an energy provider's website (is this a separate app?)
  